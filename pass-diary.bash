#!/usr/bin/env bash

[[ $# -gt 1 ]] && die "Usage: $PROGRAM $COMMAND <date, today if omitted.>"

# Convert to a date.
the_date=$(date +'%F' -d "${1:-today}")

# Convert date to folder structure & Generate pass file location.
echo "Adding or Editing diary ${the_date}"
path=".diary/${the_date//-//}"

# From this part it is the pass extension.
check_sneaky_paths "$path"
mkdir -p -v "$PREFIX/$(dirname -- "$path")"
set_gpg_recipients "$(dirname -- "$path")"
passfile="$PREFIX/$path.gpg"
set_git "$passfile"

# Temp dir stuff.
tmpdir #Defines $SECURE_TMPDIR
tmp_file="$(mktemp -u "$SECURE_TMPDIR/XXXXXX")-${path//\//-}.txt"

# Check for adding new entry, or editing an existing entry.
action="Add"
if [[ -f $passfile ]]; then
    # Decrypt the existing entry.
    $GPG -d -o "$tmp_file" "${GPG_OPTS[@]}" "$passfile" || exit 1
    action="Edit"
fi

# Edit the actual diary entry.
${EDITOR:-vi} "$tmp_file"
[[ -f $tmp_file ]] || die "New diary entry not saved."

# Save the entry if needed.
$GPG -d -o - "${GPG_OPTS[@]}" "$passfile" 2>/dev/null | diff - "$tmp_file" &>/dev/null && die "Diary entry unchanged."
while ! $GPG -e "${GPG_RECIPIENT_ARGS[@]}" -o "$passfile" "${GPG_OPTS[@]}" "$tmp_file"; do
  yesno "GPG encryption failed. Would you like to try again?"
done
git_add_file "$passfile" "$action Diary entry for $path using ${EDITOR:-vi}."