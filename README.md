# Pass Diary

An extension for the [password store](https://www.passwordstore.org/) that allows managing your diary entries in your password vault as well.
Because your diary contains far more sensitive information than a simple password.

## Usage

- To view or edit your diary entry for today: `pass diary`
- To view or edit your diary entry for a specific date: `pass diary 2024-06-11`. 
  - Relative english words can also be used to specify a date, such as `pass diary yesterday` or `pass diary "last week"`

Your diary entries will be stored in a folder called `.diary`, 
so that they won't be shown when using pass for normal passwords. 

## Installation

1. Enable password-store extensions by setting `PASSWORD_STORE_ENABLE_EXTENSIONS=true`
2. Add `pass-diary.bash` to your extension folder (by default at `~/.password-store/.extensions`)

